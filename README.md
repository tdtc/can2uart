# README #

This example uses [MCB2300(P.R.C version: LPC2368)](https://www.keil.com/mcb2300/mcb2370.asp) Evaluation Board.

### What is this repository for? ###

* Quick summary    
CAN signal I/O.
* Version    
2011.02.22

### How do I get set up? ###

* Summary of set up    
Before debugging, remove the onboard LCD (DB4, DB5, DB6, DB7).    
Because the buttons use P1.24, P1.25, P1.26 (reserved), P1.27 (reserved)
* Configuration    
OS:  Windows 32bit(Win XP SP3)    
IDE: Keil v3.5    
ICE: ULink    
PC: [USB2CAN](https://www.peak-system.com/PCAN-USB.199.0.html?&L=1)

### Who do I talk to? ###

* Repo owner or admin    
Guibin li with YT(sjz)
* Other community or team contact